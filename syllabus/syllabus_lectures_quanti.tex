% =================================================
%              L A T E X   P A P E R
%
%                by Gabriel Alcaras
% =================================================

% CHKTEX SETTINGS (see http://www.nongnu.org/chktex/ChkTeX.pdf)
% chktex-file 26 % enable spaces in front of punctuation (for French)

\documentclass[
  11pt, % Main document font size
  a4paper, % Paper type, use 'letterpaper' for US Letter paper
  oneside, % One page layout (no page indentation)
  headinclude,footinclude, % Extra spacing for the header and footer
  captions=figureheading, % Right references number in figure subfloats (https://tex.stackexchange.com/questions/98260/wrong-reference-number-for-subfloat)
  BCOR=5mm
]{scrartcl}

\input{setup_syllabus.tex}

\addbibresource{lectures_quanti.bib}

% =========================================
% HEADERS: title, author, affiliation, date
% =========================================

% The article title
\title{\normalfont\spacedallcaps{%
  Syllabus
  }\linebreak%
  Lectures en sociologie quantitative
}

% The article author(s) - author affiliations need to be specified in the AUTHOR AFFILIATIONS block
\author{\spacedlowsmallcaps{Gabriel Alcaras*}}

% An optional date to appear under the author(s)
\date{CPES 1 · 2019--2020}

% Author affiliation
\newcommand{\authoraffiliation}{%
  Contact : \texttt{\href{mailto:gabriel.alcaras@ehess.fr}{gabriel.alcaras@ehess.fr}}.
}

\definecolor{lightgray}{HTML}{565656}

% ====================
% DOCUMENT STARTS HERE
% ====================

\begin{document}

% ==========
% FIRST PAGE
% ==========

% TABLE OF CONTENTS & LISTS OF FIGURES AND TABLES

\maketitle % Print the title/author/date block

% TOC
\setcounter{tocdepth}{2} % Set the depth of the table of contents to show sections and subsections only
\tableofcontents % Print the table of contents


% {\color{lightgray}version : \gitcommithash.}

% AUTHOR AFFILIATIONS
{\let\thefootnote\relax\footnotetext{* \authoraffiliation}}

% =========================
% WRITE YOUR PAPER HERE ;-)
% =========================

\newpage
\section{Objectifs}

L'objectif du cours est double :

\begin{itemize}
  \item Former les étudiant·e·s à la lecture d'articles scientifiques en
    sciences sociales.
  \item Contribuer à une fondation solide de lecture et de compréhension du
    raisonnement quantitatif.
\end{itemize}

\section{Règles du jeu}

\subsection{Division en groupes et différents rôles}

La classe est divisée en trois groupes A, B et C. Chaque semaine, deux groupes
(par exemple A et B) doivent lire \textbf{avant le cours} le texte proposé ; le
troisième (par exemple C) n'a pas besoin de préparer la séance. Les groupes se
relaient, de sorte que chaque étudiant·e peut se dispenser de lire le texte une
fois toutes les trois semaines.

Le groupe C, qui ne lit pas le texte, a pourtant un double rôle très
important lors de la séance.

\begin{itemize}
  \item D'une part, ce groupe est en charge de la modération de la
    discussion.  Trois personnes seront tirées au hasard à chaque séance
    pour occuper trois rôles différents :
    \begin{itemize}
      \item \emph{Modération} : l'étudiant·e s'occupe de guider les débats, de
        relancer la discussion si elle s'étiole, d'aborder des aspects
        laissés dans l'ombre, de passer à un autre sujet si la discussion
        parvient à une impasse, et ainsi de suite.
      \item \emph{Tour de parole }: l'étudiant·e aide la personne qui modère. En
        notant qui a pris la parole et qui veut la prendre, c'est cette
        personne qui décide à qui revient la parole. Les tours de parole
        doivent être consignés par écrit et la feuille utilisée doit m'être
        remise à la fin du cours.
      \item \emph{Minutage} : l'étudiant·e se charge de chronométrer les
        différents tours de parole, et peut décider de donner la parole
        à une personne qui a eu moins de temps pour s'exprimer que les
        autres. Le temps est noté par écrit et la feuille doit m'être remise
        à la fin du cours.
    \end{itemize}
  \item D'autre part, ce groupe doit prendre des notes à partir des
    échanges qui ont lieu. À l'exception des trois personnes tirées au sort
    pour modérer la parole, \textbf{tou·te·s les étudiant·e·s doivent
    prendre ces notes par écrit}. Dans ce groupe, deux personnes
    seront tirées au sort à la fin du cours et devront rédiger un
    compte-rendu synthétique des enjeux principaux soulevés pendant la
    discussion. \textbf{Ce compte-rendu comptera pour 50\% de la note finale}.
\end{itemize}

Les groupes A et B, qui ont lu le texte et préparé la séance à l'avance, sont
ceux qui prendront donc la parole la plupart du temps. Les personnes de ce
groupe peuvent poser des questions à un·e étudiant·e en particulier ou
à l'ensemble des participant·e·s, y répondre, bref : discuter réellement des
enjeux du texte. Quelques rôles plus particuliers sont à noter :

\begin{itemize}
  \item \textit{Présentation} : une personne parmi le groupe A est tirée au sort
    au début de la séance. Elle a alors 5 minutes (ni plus, ni moins) pour
    expliquer au groupe qui n'a pas lu le texte les points essentiels de
    l'article. L'objectif est de permettre au groupe C de ne pas être perdu
    quand la discussion deviendra plus précise.
  \item \emph{Contradiction }: une personne parmi le groupe B est tirée au sort
    quand commence la phase de critique constructive. Elle a alors 5 minutes
    (ni plus, ni moins) pour présenter au groupe C les points faibles et les
    limites de l'article.
\end{itemize}

\subsection{But de la discussion}

Voici quelques éléments dont vous devez tenir compte durant la discussion :

\begin{itemize}
  \item N'oubliez jamais que \textbf{vous parlez d'abord pour le groupe qui
    n'a pas lu le texte}, et ensuite aux autres personnes qui en ont une
    connaissance détaillée. Soyez pédagogue, citez les pages, les tableaux,
    etc.
  \item Gardez à l'esprit que le cours a pour objet \textbf{les méthodes
    quantitatives et leur utilisation dans l'administration de la preuve
    scientifique}. La discussion doit évidemment aborder le thème général de
    l'article, ses questions et ses principaux résultats, mais ne doit jamais
    perdre de vue l'utilisation des statistiques.
  \item La discussion doit couvrir les thèmes suivants afin de permettre au
    groupe C de s'en faire une idée globale : \textbf{objet} (thème général,
    discipline, etc.) ; \textbf{théorie} (principaux concepts, références
    théoriques) ; \textbf{méthodes} (tableaux, graphiques, noms des traitements
    statistiques, variables, etc.) ; \textbf{données} (base de données,
    sources, entretiens, etc.). La discussion doit rendre compte de ces
    différents éléments et de leur articulation.
\end{itemize}

\subsection{Déroulement type d'une séance}

La discussion dure au minimum 1h15 et au maximum 1h30. L'étudiant·e en charge
de la modération doit faire respecter le temps imparti à chaque partie de la
discussion. Cette dernière se déroule comme suit :

\begin{enumerate}
  \item \textbf{Avant la discussion}. On tire au sort les trois personnes de
    l'équipe de modération ainsi que la personne qui fait l'exposé de
    présentation de 5 minutes. Le trinôme de modération s'assoit à côté les
    un·e·s des autres.
  \item \textbf{Introduction de la présentation }(5mn). Voir \emph{Présentation}.
  \item \textbf{Présentation }(20--30mn). La première partie de la discussion
    doit permettre d'explorer les principaux aspects abordés par l'article. On
    peut ouvrir aux questions du groupe C avant de passer à la contradiction.
  \item \textbf{Introduction de la contradiction }(5mn). Voir \emph{Contradiction}.
  \item \textbf{Contradiction }(30--40mn). La seconde partie de la discussion
    a pour but de discuter des limites, des points négatifs, des points
    épistémologiques en lien avec le texte. On peut ouvrir aux questions du
    groupe C avant de passer à la conclusion.
  \item \textbf{Conclusion} (5mn). Effectuée par le professeur, pour souligner
    les points les plus importants ou des points qui n'auraient pas été
    abordés.
  \item \textbf{Après la discussion}. L'équipe de modération rend ses fiches
    (tour de parole, minutage). On tire au sort les personnes qui, parmi le
    groupe C, devront faire un compte-rendu synthétique des discussions.
\end{enumerate}

\subsection{Notes sur le tirage au sort}

Le tirage au sort est effectué le jour de la séance : ni les étudiant·e·s ni le
professeur ne savent à l'avance qui occupera quel rôle.

Quelques règles viennent encadrer ce tirage au sort :

\begin{itemize}
  \item Si un·e étudiant·e est tiré·e au sort pour rendre une note de synthèse
    lors de la séance suivante :
    \begin{itemize}
      \item Elle ne pourra plus être tirée au sort pour rédiger une autre note
        de synthèse.
      \item Elle sera, pour la séance du rendu seulement, temporairement exclue
        du tirage au sort pour les rôles d'introduction de la présentation et
        de la contradiction.
    \end{itemize}
  \item Si un·e étudiant·e est tiré·e au sort dans un autre rôle (modération,
    présentation, etc.), il peut toujours être tiré au sort (y compris la fois
    suivante) dans le même rôle. Toutefois, par souci d'équité, il aura deux
    fois moins de chances d'être sélectionné qu'une personne qui n'a pas encore
    assumé ce rôle.
\end{itemize}

\subsection{Règles générales}

\paragraph{Coopération} N'oubliez pas que \emph{chaque discussion doit
s'effectuer dans un esprit coopératif}. L'idée est d'avoir la meilleure
discussion possible sur un plan scientifique pour produire un document commun,
le compte-rendu. Il ne s'agit pas de «~gagner~» un argument ni même
nécessairement de convaincre l'autre personne.

\paragraph{Audience} \textit{N'oubliez pas que la discussion a lieu avant tout
pour le groupe C} qui, contrairement à vous, n'a pas lu le texte. Il faut donc
s'assurer d'être clair·e pour une personne qui ne connait rien au sujet :
définir certains termes si nécessaire, donner les numéros de page, expliquer
comment lire un tableau un peu compliqué, etc.

\paragraph{Citation} Quand un·e participant·e développe un argument ou commente
le texte, \textit{il est impératif de donner la page et/ou le numéro du tableau
ou du graphique}, pour que tout le monde puisse suivre (en particulier le
groupe C). De manière générale, il est important de donner des exemples précis
tirés de l'article quand on prend la parole.

\paragraph{Prise de parole} \textit{Il faut lever la main} pour se signaler
à la personne qui prend les tours de parole, et \textit{attendre son tour} avant de
parler. Il ne faut surtout pas interrompre la personne en train de s'exprimer,
seuls le trinôme de modération et le professeur peuvent interrompre une prise
de parole. Toute interruption intempestive sera sanctionnée.

\paragraph{Coupe-file} Si un·e participant·e souhaite répondre immédiatement à une
intervention en cours, \textit{il est possible d'utiliser un coupe-file une
fois (et une seule fois) par discussion} pour prendre la parole tout de suite
après la fin de cette intervention. Le coupe-file s'emploie en tapant deux fois
sur sa table.

\paragraph{Tirage au sort} Le tirage au sort est réalisé par le professeur, par
informatique. Au départ, chaque personne à l'intérieur d'un groupe a autant de
chance d'être choisie qu'un·e autre. Au fur et à mesure des tirages, les
personnes ayant déjà été tirées au sort pour un rôle auront moins de chances
d'être choisies~--- ce qui ne veut pas dire qu'elles ne seront plus jamais
sélectionnées pour assumer ce rôle. Par exemple, imaginons que nous tirons au
sort le rôle de modération lors de la première séance. Tout le monde a une
chance égale d'être sélectionné·e. Si Angela est choisie pour modérer, elle aura
deux fois moins de chances d'être modératrice la séance suivante. Autrement
dit, \textit{ce n'est pas parce qu'une personne a rempli un rôle une fois
qu'elle ne l'occupera plus par la suite}, c'est juste bien moins probable.

\subsection{Validation et évaluations}

L'évaluation du cours est individuelle. Elle repose à 50\% sur la participation
en classe et à 50\% sur la note de synthèse.

\paragraph{Participation} La note de participation évalue moins le contenu des
interventions (il est par exemple possible de se tromper dans la lecture de
tableaux statistiques et d'obtenir une bonne note) que leur forme. Elle cherche
surtout à vérifier que l'étudiant·e a joué le jeu : prise de parole régulière
mais respectueuse, rôle de modérateur·ice correctement assuré, et ainsi de
suite.

\paragraph{Note de synthèse} Rédigée par un·e étudiant·e du groupe C, elle doit
rendre compte du texte discuté \textit{uniquement à travers les débats qui se
sont tenus en classe}. Il n'est donc pas nécessaire de relire le texte en
détail pour la rédiger : vos notes (et, éventuellement, celles de vos
camarades) doivent être suffisantes. Sur la forme, vous pouvez la structurer
comme vous le souhaitez, par exemple en commençant par présenter le texte, puis
en donnant quelques critiques ou limites. \textit{Le texte ne doit pas dépasser
les 10 000 signes, espaces comprises}, soit environ 2,5 pages sans interlignes.
Notez que si vous êtes tiré·e au sort pour rédiger une note de synthèse, vous
ne pouvez pas être sélectionnée la séance suivante pour présenter ou critiquer.

\subsection{Explication des rôles}

\paragraph{Modération} La modération est un rôle très important lors de la
discussion, puisqu'il faut veiller à son bon déroulement. Premièrement, il faut
gérer \emph{la temporalité de la discussion}, en s'assurant qu'elle dure au moins 1h15
et au plus 1h30, et que les temps alloués à la présentation et à la
contradiction sont équilibrés. Deuxièmement, il faut faire respecter \emph{l'équité
de la prise de parole}, avec l'aide des deux autres membres du binôme. Pour
faire respecter ces deux premiers points, il est possible de (poliment)
interrompre une intervention. Enfin, et c'est sans doute le plus important,
\emph{la personne représente son groupe}, qui n'a pas lu le texte discuté. Il
ne faut donc pas hésiter à demander des précisions et à poser des questions
pour se faire une meilleure idée de l'article et de son contenu.  De manière
générale, un·e modérateur·ice gère le flux de la discussion, en relançant avec
une question si la conversation s'essouffle, en abordant des aspects
inexplorés, en revenant sur un point oublié ou en passant à un autre sujet si
le débat parvient à une impasse.

\paragraph{Tour de parole} La gestion du tour de parole a pour objectif de
\emph{permettre aux personnes qui se sont le moins exprimées d'intervenir en
priorité}. Au début de la discussion, chaque personne a la même priorité. La
file d'attente pour prendre la parole se fait dans l'ordre des mains levées.
La personne qui gère la parole inscrit les personnes qui lèvent la main dans
l'ordre où elle les voit, et leur adresse à chacune un petit signe de tête pour
leur signifier qu'elles ont bien été inscrites sur la liste d'attente. Une fois
qu'une personne a pris la parole, elle perd un point de priorité. Autrement
dit, quand elle lèvera la main une nouvelle fois pour prendre la parole, elle
passera après les personnes qui ne se sont pas encore exprimées. Une personne
qui s'est exprimée deux fois passe après celles qui se sont exprimées une fois,
et ainsi de suite. Seule exception, le recours à un coupe-file (la personne
tape deux fois sur sa table) permet de passer en haut de la file d'attente,
quelque soit son niveau de priorité. La personne en charge de ce rôle donne la
parole en nommant la personne, elle peut aussi annoncer quelles sont les
personnes inscrites sur la liste. Elle peut aussi interrompre une intervention
si quelqu'un parle sans attendre son tour. Les tours de parole doivent être
notés (voir ci-dessous) ; la feuille sera collectée à la fin de la discussion.

\begin{table}[!ht]
  \centering
  \begin{tabular}{@{}llll@{}}
  \toprule
         Coupe-file & Tour 1 & Tour 2 & Tour 3 \\
   \midrule
         & \sout{Angela} & Pierre &\\
         & \sout{Pierre} & & \\
         & \sout{Constance} & & \\
         & Karim & & \\
   \bottomrule \\
  \end{tabular}

   {\small\emph{Explication : Angela, Pierre et Constance ont levé la main pour
   prendre la parole. Angela et Pierre ont déjà parlé, Constance est en train
   d'intervenir. Pendant qu'elle parle, Karim et Pierre lèvent la main à leur
   tour. Comme Pierre a déjà parlé, il est inscrit dans la deuxième colonne ;
   Karim est inscrit dans la première et passera avant lui. Avec ce système, la
   prochaine personne à parler est toujours celle qui, parmi les noms non
   barrés, se trouve en haut de la colonne le plus à gauche.}}
   \caption{Comment prendre les tours de parole}
\end{table}

\paragraph{Minutage} La personne en charge du minutage note le temps pris par
chaque personne pour s'exprimer. \emph{L'objectif est avant tout documentaire, mais
peut aussi servir à influer sur le tour de parole}. Par exemple, entre une personne
qui s'est exprimée deux fois pour un temps total de 2 minutes et une autre qui
ne s'est exprimée qu'une fois mais a parlé pendant 10 minutes, la personne en
charge du minutage peut donner la priorité à la première. Le comptage
s'effectue à la minute près, en faisant à peu près une encoche par minute
à coté du nom de l'intervenant·e. Le document sera collecté à la fin de la
séance.

\begin{table}[!ht]
  \centering
  \begin{tabular}{@{}rl@{}}
  \toprule
         Nom & Temps \\
   \midrule
         Angela & ||| \\
         Pierre  & ||||||| \\
         Constance & || \\
         Karim & \\
   \bottomrule \\
  \end{tabular}

   {\small\emph{Explication : Angela a parlé environ 3 minutes, Pierre environ
       7 et Constance 2.}}
   \caption{Comment noter les temps de parole}
\end{table}

\paragraph{Présentation} L'objectif de la présentation (à travers l'exposé de
5 minutes et la discussion qui suit) est de donner au groupe C, qui n'a pas lu
le texte, une idée précise de l'article. Pour ce faire, l'idéal est d'aller du
général au particulier : qui est l'auteur·e, quel est le thème général de
l'article, quelles sont les principales références théoriques, quels sont ses
principaux résultats, comment ces résultats sont obtenus (méthode), etc.
N'hésitez pas à donner votre avis, à souligner les points qui vous ont paru les
plus intéressants ou les moins convaincants.

\paragraph{Contradiction} L'objectif de la contradiction (à travers l'exposé de
5 minutes et la discussion qui suit) est non pas seulement de pointer du doigt
des limites ou des failles dans l'argumentaire, mais également de faire des
propositions constructives qui permettraient de dépasser les écueils d'un
texte.

% \paragraph{Compte rendu synthétique}

\section{Séances}

\subsection{Séance introductive}

Présentation du cours et méthodologie de lecture.

\subsection{Comment rendre visible le travail gratuit des femmes ?}

Texte : \fullcite{Chadeau1981}.

\subsection{La tradition du mariage a-t-elle fait son temps ?}

Texte : \fullcite{Maillochon2019}.

\subsection{Les agressions racistes sont-elles des actes de «~défense~» ?}

Texte : \fullcite{hajjat2019psdr}.

\subsection{Internet renforce-t-il la mixité sociale ?}

Texte : \fullcite{Bergstrom2016}.

\subsection{Peut-on mettre le genre dans des cases ?}

Texte : \fullcite{Trachman2018}.

\subsection{Que sont les soixante-huitard·e·s devenu·e·s ?}

Texte : \fullcite{Pagis2011}.

\subsection{Qu'est-ce que vivre une crise économique ?}

Texte : \fullcite{Blavier2018}.

\subsection{Le rap s'est-il fait par featuring ?}

Texte : \fullcite{Hammou2009}.

\subsection{Facebook, pour quoi faire ?}

Texte : \fullcite{Bastard2017}.

\subsection{Henri IV fait-il grimper les prix de l'immobilier ?}

Texte : \fullcite{Fack2009}.

\subsection{Les femmes sortent-elles la «~peur au ventre~» ?}

Texte : \fullcite{Condon2005}.

\subsection{Les enfants d'immigré·e·s ont-ils le même destin scolaire ?}

Texte : \fullcite{Ichou2013}.

\subsection{Comment expliquer la financiarisation de l'économie française ?}

Texte : \fullcite{Lemercier2016}.

\subsection{Conclusion : quelles relations entre sociologie et statistique ?}

Texte : \fullcite{Desrosieres2001}.

\newpage
\section{Calendrier synthétique}

\begin{table}[!ht]
  \begin{tabular}{@{}lllll@{}}
  \toprule
  \# & Date & Prés. & Contra. & Texte à lire \\
   \midrule
  01 & 03 fév. & A & B &~--- \\[1em]
  02 & 24 fév. & B & C &~\cite{Chadeau1981} \\
  03 & 02 mar. & C & A &~\cite{Maillochon2019} \\
  04 & 09 mar. & A & B &~\cite{hajjat2019psdr} \\
  05 & 16 mar. & B & C &~\cite{Bergstrom2016} \\
  06 & 23 mar. & C & A &~\cite{Trachman2018} \\
  07 & 30 mar. & A & B &~\cite{Pagis2011} \\[1em]
  08 & 20 avr. & B & C &~\cite{Blavier2018} \\
  09 & 27 avr. & C & A &~\cite{Hammou2009}\\
  10 & 04 mai & A & B &~\cite{Bastard2017} \\
  11 & 11 mai & B & C &~\cite{Fack2009} \\
  12 & 18 mai & C & A &~\cite{Condon2005}\\
  13 & 25 mai & A & B &~\cite{Ichou2013}\\
  14 & 01 juin & B & C &~\cite{Lemercier2016} \\
  15 & 15 juin & C & A &~\cite{Desrosieres2001} \\
   \bottomrule
  \end{tabular}
\end{table}

\end{document}
