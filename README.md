# Lectures en sociologie quantitative

## Répartition des groupes

GROUPE A :

- Ana
- Jordan
- Jade
- Ella
- Youssef
- Maëlle
- Angela
- Ayoub

GROUPE B :

- Juliette
- Emma
- Clara
- Violaine
- Maud
- Jules
- Lola
- Coline

GROUPE C :

- Edmond
- Lisa
- Héloïse
- Emilie
- Lily
- Rosa
- Saioa
